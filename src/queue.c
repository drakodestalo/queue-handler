#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "queue.h"

#define ALLOC_SET(var, value) \
	var = (value *) malloc(sizeof(value));	\
	memset(var,0,sizeof(value))

/* build_queue
 *
 * This function creates a new queue, it requires:
 * - a consumer function which takes the payload as a void pointer
 * - a payload cleaner function
 * */
t_queue_def build_queue( int(*consumer)(const void*), void (*payload_cleaner)(void *) ) {
	t_queue_def new_queue;

	ALLOC_SET(new_queue, struct queue_def);

	new_queue->q_root = NULL;
	new_queue->q_last = NULL;
	new_queue->consumer = consumer;
	new_queue->payload_cleaner = payload_cleaner;
	new_queue->elems = 0;
	new_queue->lock = 0;

	return new_queue;
}

int add_queue(t_queue_def queue, const void *payload) {
	struct queue_t *nitem;
	
	assert(queue != NULL);
	
	ALLOC_SET(nitem, struct queue_t);
	nitem->payload = (void *)payload;
	nitem->next = NULL;
	
	if (queue->q_root == NULL) {
		// First element of the queue
		queue->q_root = queue->q_last = nitem;
		
	} else {
		// Add new element at the end
		queue->q_last->next = nitem;
		queue->q_last = nitem;
	}
	
	queue->elems++;

	return 0;
}

/* process_queue
 *
 * This function process an item from the queue
 * */

int process_queue(t_queue_def queue) {
	struct queue_t *item;

	if (queue->q_root) {
		while (queue->lock != 0) ;
		
		queue->lock = 1;
		// First, we dequeue the element
		item = queue->q_root;
		queue->q_root = queue->q_root->next;
		queue->lock = 0; // Release the lock

		// Now, let's process it
		queue->consumer(item->payload);

		// Clean the payload
		queue->payload_cleaner(item->payload);

		// Clear the item
		free (item);
		queue->elems--;
	}
	return 0;
}

void _free_queue_item (struct queue_t *item ) {
	if (item != NULL) {
		_free_queue_item(item->next);
		free(item);
		item = NULL;
	}
}

void release_queue(t_queue_def queue) {
	if (queue != NULL) _free_queue_item(queue->q_root);
}