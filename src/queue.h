/*
 * queue.h
 *
 *  Created on: 08/03/2015
 *      Author: esteb_000
 */

#ifndef QUEUE_H_
#define QUEUE_H_

struct queue_t {
	
	struct queue_t *next;
	void *payload;
};

struct queue_def {
	unsigned long elems;
	unsigned int lock;
	struct queue_t *q_root;
	struct queue_t *q_last;
	int (*consumer)(const void*); 	// The parameter of this function should interpret
									// and process correctly the "payload" parameter
	void (*payload_cleaner)(void *);
};

typedef struct queue_def *t_queue_def;

t_queue_def build_queue(
		int(*consumer)(const void*),
		void (*payload_cleaner)(void *)
		);

int add_queue(t_queue_def queue, const void *payload);

int process_queue(t_queue_def queue);

void release_queue(t_queue_def queue);


#endif /* QUEUE_H_ */